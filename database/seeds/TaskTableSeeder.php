<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([[
            'id'=>1,
            'title'=>'test task 1',
            'user_id' => 1,
            'status'=> 0,
            ],
            [
            'id'=>2,
            'title'=>'test task 2',
            'user_id' => 1,
            'status'=> 0,
            ],
            [
            'id'=>3,
            'title'=>'test task 3',
            'user_id' => 1,
            'status'=> 0,
            ],
            ]);
    }
}
