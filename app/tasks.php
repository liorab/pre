<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tasks extends Model
{
    protected $fillable = [
        'title','status'
    ];
    
    public function user(){
        return $this->hasMany('App\user');
    }
}
